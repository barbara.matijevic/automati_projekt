﻿namespace KALKULATOR
{
    partial class Calculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_equally = new System.Windows.Forms.Button();
            this.btn_sub = new System.Windows.Forms.Button();
            this.btn_pls = new System.Windows.Forms.Button();
            this.btn_decimal = new System.Windows.Forms.Button();
            this.btn_divide = new System.Windows.Forms.Button();
            this.btn_sqrt = new System.Windows.Forms.Button();
            this.btn_percent = new System.Windows.Forms.Button();
            this.btn_multi = new System.Windows.Forms.Button();
            this.btn_clear = new System.Windows.Forms.Button();
            this.result = new System.Windows.Forms.TextBox();
            this.btn_7 = new System.Windows.Forms.Button();
            this.btn_8 = new System.Windows.Forms.Button();
            this.btn_4 = new System.Windows.Forms.Button();
            this.btn_1 = new System.Windows.Forms.Button();
            this.btn_2 = new System.Windows.Forms.Button();
            this.btn_5 = new System.Windows.Forms.Button();
            this.btn_9 = new System.Windows.Forms.Button();
            this.btn_6 = new System.Windows.Forms.Button();
            this.btn_3 = new System.Windows.Forms.Button();
            this.btn_0 = new System.Windows.Forms.Button();
            this.temp = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_equally
            // 
            this.btn_equally.BackColor = System.Drawing.Color.Gold;
            this.btn_equally.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_equally.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_equally.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_equally.Location = new System.Drawing.Point(204, 234);
            this.btn_equally.Name = "btn_equally";
            this.btn_equally.Size = new System.Drawing.Size(42, 57);
            this.btn_equally.TabIndex = 12;
            this.btn_equally.Text = "=";
            this.btn_equally.UseVisualStyleBackColor = false;
            this.btn_equally.Click += new System.EventHandler(this.btn_equally_Click);
            // 
            // btn_sub
            // 
            this.btn_sub.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_sub.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_sub.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_sub.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_sub.Location = new System.Drawing.Point(204, 172);
            this.btn_sub.Name = "btn_sub";
            this.btn_sub.Size = new System.Drawing.Size(42, 25);
            this.btn_sub.TabIndex = 13;
            this.btn_sub.Text = "-";
            this.btn_sub.UseVisualStyleBackColor = false;
            this.btn_sub.Click += new System.EventHandler(this.operators);
            // 
            // btn_pls
            // 
            this.btn_pls.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_pls.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_pls.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_pls.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_pls.Location = new System.Drawing.Point(204, 203);
            this.btn_pls.Name = "btn_pls";
            this.btn_pls.Size = new System.Drawing.Size(42, 25);
            this.btn_pls.TabIndex = 14;
            this.btn_pls.Text = "+";
            this.btn_pls.UseVisualStyleBackColor = false;
            this.btn_pls.Click += new System.EventHandler(this.operators);
            // 
            // btn_decimal
            // 
            this.btn_decimal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_decimal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_decimal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_decimal.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_decimal.Location = new System.Drawing.Point(27, 265);
            this.btn_decimal.Name = "btn_decimal";
            this.btn_decimal.Size = new System.Drawing.Size(42, 25);
            this.btn_decimal.TabIndex = 15;
            this.btn_decimal.Text = ",";
            this.btn_decimal.UseVisualStyleBackColor = false;
            this.btn_decimal.Click += new System.EventHandler(this.numbers);
            // 
            // btn_divide
            // 
            this.btn_divide.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_divide.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_divide.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_divide.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_divide.Location = new System.Drawing.Point(145, 266);
            this.btn_divide.Name = "btn_divide";
            this.btn_divide.Size = new System.Drawing.Size(42, 25);
            this.btn_divide.TabIndex = 16;
            this.btn_divide.Text = "÷";
            this.btn_divide.UseVisualStyleBackColor = false;
            this.btn_divide.Click += new System.EventHandler(this.operators);
            // 
            // btn_sqrt
            // 
            this.btn_sqrt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_sqrt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_sqrt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_sqrt.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_sqrt.Location = new System.Drawing.Point(86, 141);
            this.btn_sqrt.Name = "btn_sqrt";
            this.btn_sqrt.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btn_sqrt.Size = new System.Drawing.Size(42, 25);
            this.btn_sqrt.TabIndex = 17;
            this.btn_sqrt.Text = "√ ̅";
            this.btn_sqrt.UseVisualStyleBackColor = false;
            this.btn_sqrt.Click += new System.EventHandler(this.btn_sqrt_Click);
            // 
            // btn_percent
            // 
            this.btn_percent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_percent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_percent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_percent.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_percent.Location = new System.Drawing.Point(145, 141);
            this.btn_percent.Name = "btn_percent";
            this.btn_percent.Size = new System.Drawing.Size(42, 25);
            this.btn_percent.TabIndex = 18;
            this.btn_percent.Text = "x^2";
            this.btn_percent.UseVisualStyleBackColor = false;
            this.btn_percent.Click += new System.EventHandler(this.btn_percent_Click);
            // 
            // btn_multi
            // 
            this.btn_multi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_multi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_multi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_multi.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_multi.Location = new System.Drawing.Point(204, 141);
            this.btn_multi.Name = "btn_multi";
            this.btn_multi.Size = new System.Drawing.Size(42, 25);
            this.btn_multi.TabIndex = 19;
            this.btn_multi.Text = "*";
            this.btn_multi.UseVisualStyleBackColor = false;
            this.btn_multi.Click += new System.EventHandler(this.operators);
            // 
            // btn_clear
            // 
            this.btn_clear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_clear.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_clear.Location = new System.Drawing.Point(27, 141);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(42, 25);
            this.btn_clear.TabIndex = 20;
            this.btn_clear.Text = "CE";
            this.btn_clear.UseVisualStyleBackColor = false;
            this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
            // 
            // result
            // 
            this.result.BackColor = System.Drawing.Color.Gray;
            this.result.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.result.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.result.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.result.Location = new System.Drawing.Point(27, 59);
            this.result.Margin = new System.Windows.Forms.Padding(5);
            this.result.Multiline = true;
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(219, 55);
            this.result.TabIndex = 52;
            this.result.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btn_7
            // 
            this.btn_7.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_7.Location = new System.Drawing.Point(27, 173);
            this.btn_7.Name = "btn_7";
            this.btn_7.Size = new System.Drawing.Size(42, 25);
            this.btn_7.TabIndex = 22;
            this.btn_7.Text = "7";
            this.btn_7.UseVisualStyleBackColor = false;
            this.btn_7.Click += new System.EventHandler(this.numbers);
            // 
            // btn_8
            // 
            this.btn_8.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_8.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_8.Location = new System.Drawing.Point(86, 172);
            this.btn_8.Name = "btn_8";
            this.btn_8.Size = new System.Drawing.Size(42, 25);
            this.btn_8.TabIndex = 23;
            this.btn_8.Text = "8";
            this.btn_8.UseVisualStyleBackColor = false;
            this.btn_8.Click += new System.EventHandler(this.numbers);
            // 
            // btn_4
            // 
            this.btn_4.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_4.Location = new System.Drawing.Point(27, 204);
            this.btn_4.Name = "btn_4";
            this.btn_4.Size = new System.Drawing.Size(42, 25);
            this.btn_4.TabIndex = 24;
            this.btn_4.Text = "4";
            this.btn_4.UseVisualStyleBackColor = false;
            this.btn_4.Click += new System.EventHandler(this.numbers);
            // 
            // btn_1
            // 
            this.btn_1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_1.Location = new System.Drawing.Point(27, 234);
            this.btn_1.Name = "btn_1";
            this.btn_1.Size = new System.Drawing.Size(42, 25);
            this.btn_1.TabIndex = 25;
            this.btn_1.Text = "1";
            this.btn_1.UseVisualStyleBackColor = false;
            this.btn_1.Click += new System.EventHandler(this.numbers);
            // 
            // btn_2
            // 
            this.btn_2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_2.Location = new System.Drawing.Point(86, 234);
            this.btn_2.Name = "btn_2";
            this.btn_2.Size = new System.Drawing.Size(42, 25);
            this.btn_2.TabIndex = 26;
            this.btn_2.Text = "2";
            this.btn_2.UseVisualStyleBackColor = false;
            this.btn_2.Click += new System.EventHandler(this.numbers);
            // 
            // btn_5
            // 
            this.btn_5.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_5.Location = new System.Drawing.Point(86, 203);
            this.btn_5.Name = "btn_5";
            this.btn_5.Size = new System.Drawing.Size(42, 25);
            this.btn_5.TabIndex = 27;
            this.btn_5.Text = "5";
            this.btn_5.UseVisualStyleBackColor = false;
            this.btn_5.Click += new System.EventHandler(this.numbers);
            // 
            // btn_9
            // 
            this.btn_9.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_9.Location = new System.Drawing.Point(145, 172);
            this.btn_9.Name = "btn_9";
            this.btn_9.Size = new System.Drawing.Size(42, 25);
            this.btn_9.TabIndex = 28;
            this.btn_9.Text = "9";
            this.btn_9.UseVisualStyleBackColor = false;
            this.btn_9.Click += new System.EventHandler(this.numbers);
            // 
            // btn_6
            // 
            this.btn_6.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_6.Location = new System.Drawing.Point(145, 203);
            this.btn_6.Name = "btn_6";
            this.btn_6.Size = new System.Drawing.Size(42, 25);
            this.btn_6.TabIndex = 29;
            this.btn_6.Text = "6";
            this.btn_6.UseVisualStyleBackColor = false;
            this.btn_6.Click += new System.EventHandler(this.numbers);
            // 
            // btn_3
            // 
            this.btn_3.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_3.Location = new System.Drawing.Point(145, 234);
            this.btn_3.Name = "btn_3";
            this.btn_3.Size = new System.Drawing.Size(42, 25);
            this.btn_3.TabIndex = 30;
            this.btn_3.Text = "3";
            this.btn_3.UseVisualStyleBackColor = false;
            this.btn_3.Click += new System.EventHandler(this.numbers);
            // 
            // btn_0
            // 
            this.btn_0.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_0.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_0.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_0.Location = new System.Drawing.Point(86, 267);
            this.btn_0.Name = "btn_0";
            this.btn_0.Size = new System.Drawing.Size(42, 25);
            this.btn_0.TabIndex = 31;
            this.btn_0.Text = "0";
            this.btn_0.UseVisualStyleBackColor = false;
            this.btn_0.Click += new System.EventHandler(this.numbers);
            // 
            // temp
            // 
            this.temp.AutoSize = true;
            this.temp.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.temp.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temp.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.temp.Location = new System.Drawing.Point(211, 27);
            this.temp.Name = "temp";
            this.temp.Size = new System.Drawing.Size(0, 18);
            this.temp.TabIndex = 32;
            // 
            // Calculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(274, 325);
            this.Controls.Add(this.temp);
            this.Controls.Add(this.btn_0);
            this.Controls.Add(this.btn_3);
            this.Controls.Add(this.btn_6);
            this.Controls.Add(this.btn_9);
            this.Controls.Add(this.btn_5);
            this.Controls.Add(this.btn_2);
            this.Controls.Add(this.btn_1);
            this.Controls.Add(this.btn_4);
            this.Controls.Add(this.btn_8);
            this.Controls.Add(this.btn_7);
            this.Controls.Add(this.result);
            this.Controls.Add(this.btn_clear);
            this.Controls.Add(this.btn_multi);
            this.Controls.Add(this.btn_percent);
            this.Controls.Add(this.btn_sqrt);
            this.Controls.Add(this.btn_divide);
            this.Controls.Add(this.btn_decimal);
            this.Controls.Add(this.btn_pls);
            this.Controls.Add(this.btn_sub);
            this.Controls.Add(this.btn_equally);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.Name = "Calculator";
            this.Text = "Calculator";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Load += new System.EventHandler(this.Calculator_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_equally;
        private System.Windows.Forms.Button btn_sub;
        private System.Windows.Forms.Button btn_pls;
        private System.Windows.Forms.Button btn_decimal;
        private System.Windows.Forms.Button btn_divide;
        private System.Windows.Forms.Button btn_sqrt;
        private System.Windows.Forms.Button btn_percent;
        private System.Windows.Forms.Button btn_multi;
        private System.Windows.Forms.Button btn_clear;
        private System.Windows.Forms.TextBox result;
        private System.Windows.Forms.Button btn_7;
        private System.Windows.Forms.Button btn_8;
        private System.Windows.Forms.Button btn_4;
        private System.Windows.Forms.Button btn_1;
        private System.Windows.Forms.Button btn_2;
        private System.Windows.Forms.Button btn_5;
        private System.Windows.Forms.Button btn_9;
        private System.Windows.Forms.Button btn_6;
        private System.Windows.Forms.Button btn_3;
        private System.Windows.Forms.Button btn_0;
        private System.Windows.Forms.Label temp;
    }
}

